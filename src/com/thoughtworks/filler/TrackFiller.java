package com.thoughtworks.filler;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.exception.NotEnoughTracksException;
import com.thoughtworks.exception.TrackFillerException;
import com.thoughtworks.model.Event;
import com.thoughtworks.model.Track;

public class TrackFiller {

	public void fill(List<Track> tracks, List<Event> events) {
		if(tracks == null || tracks.isEmpty() || events == null){
			throw new TrackFillerException();
		}

		List<Event> remainingEvents = new ArrayList<Event>();
		for (int trackIndex = 0; trackIndex < tracks.size(); trackIndex++) {
			for (Event event : events) {
				Track currentTrack = tracks.get(trackIndex);
				if(!currentTrack.addEvent(event)){
					remainingEvents.add(event);
				}
			}
			
			events = remainingEvents;
			remainingEvents = new ArrayList<Event>();
		}
		
		if(events.size() > 0){
			throw new NotEnoughTracksException();
		}
		
	}

}