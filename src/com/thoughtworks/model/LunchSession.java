package com.thoughtworks.model;

public class LunchSession extends Session {

	public LunchSession() {
		this.add(new Event("Lunch", getDuration()));
	}
	
	@Override
	protected int getDuration() {
		return 60;
	}
	
	@Override
	public int getStartingHour() {
		return 12;
	}
	
	@Override
	public boolean isFixed() {
		return true;
	}
	
}