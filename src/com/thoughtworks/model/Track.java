package com.thoughtworks.model;

import java.util.ArrayList;
import java.util.List;

public class Track {

	private String name;
	private MorningSession morning = new MorningSession();
	private LunchSession lunch = new LunchSession();
	private AfternoonSession afternoon = new AfternoonSession();
	private ExtraSession extra = new ExtraSession();
	private NetworkingSession networking = new NetworkingSession(this);
	
	public Track() {
		super();
	}
	
	public Track(String name) {
		super();
		this.name = name;
	}

	public Session getMorning() {
		return morning;
	}

	public LunchSession getLunch() {
		return lunch;
	}

	public Session getAfternoon() {
		return afternoon;
	}

	public NetworkingSession getNetworking() {
		return networking;
	}
	
	public String getName() {
		return this.name;
	}
	
	public ExtraSession getExtra() {
		return extra;
	}

	public List<Session> getSessions() {
		List<Session> sessions = new ArrayList<Session>();
		sessions.add(morning);
		sessions.add(lunch);
		sessions.add(afternoon);
		if(!extra.getEvents().isEmpty()){
			sessions.add(extra);
		}
		sessions.add(networking);
		return sessions;
	}
	
	public boolean addEvent(Event event) {
		for (Session session : getSessions()) {
			if(session.add(event)){
				return true;
			}
		}
		return false;
	}

	public boolean isFull() {
		return afternoon.isFull();
	}

	public void addExtraEvent(Event event) {
		if(!extra.isFull()){
			extra.add(event);
			//networking.increaseStartingHour(event.getMinutes());
		}
	}

	public Session getCurrentSession() {
		for (Session session : getSessions()) {
			if(!session.isFull())
				return session;
		}
		return null;
	}
	
}