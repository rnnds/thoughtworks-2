package com.thoughtworks.model;

public class MorningSession extends Session {

	@Override
	public int getStartingHour() {
		return 9;
	}
	
}