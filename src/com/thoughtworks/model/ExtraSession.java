package com.thoughtworks.model;

public class ExtraSession extends Session {

	@Override
	public int getStartingHour() {
		return 14;
	}
	
	@Override
	protected int getDuration() {
		return 60;
	}

}
