package com.thoughtworks.model;


public class Event implements Comparable<Event>{

	public static final int LIGHTNING_DURATION = 5;
	
	private String name;
	private int minutes;

	public Event(String name, int minutes) {
		super();
		this.name = name;
		this.minutes = minutes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	@Override
	public int compareTo(Event o) {
		return o.getMinutes() - this.getMinutes();
	}

}