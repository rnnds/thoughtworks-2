package com.thoughtworks.model;

public class AfternoonSession extends Session {

	@Override
	public int getStartingHour() {
		return 13;
	}
	
	protected int getDuration() {
		return 240; //4 hours * 60 minutes;
	}

}