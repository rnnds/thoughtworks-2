package com.thoughtworks.model;

public class NetworkingSession extends Session {

	public static final int MIN_STARTING_TIME = 16;
	
	private Track track;
	
	public NetworkingSession(Track track) {
		this.add(new Event("Networking Event", getDuration()));
		this.track = track;
	}
	
	public Track getTrack() {
		return track;
	}

	@Override
	protected int getDuration() {
		return 60;
	}

	@Override
	public int getStartingHour() {
		return 17;
	}
	
	@Override
	public boolean isFixed() {
		return true;
	}

}