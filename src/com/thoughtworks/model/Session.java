package com.thoughtworks.model;

import java.util.ArrayList;
import java.util.List;

public abstract class Session {
	
	private List<Event> events = new ArrayList<Event>();

	public int getRemainingTime() {
		return getDuration() - getEventsTotalDuration();
	}

	public int getEventsTotalDuration() {
		int totalDuration = 0;
		for (Event event : events) {
			totalDuration += event.getMinutes();
		}
		return totalDuration;
	}

	protected int getDuration() {
		return 180; //3 hours * 60 minutes;
	}

	public boolean add(Event event) {
		if(isFull() || event.getMinutes() > getRemainingTime()){
			return false;
		}
		return events.add(event);
	}

	public boolean isFull() {
		return this.getRemainingTime() <= Event.LIGHTNING_DURATION;
	}

	public List<Event> getEvents() {
		return events;
	}

	public boolean isFixed(){
		return false;
	}
	
	public abstract int getStartingHour();
	
}