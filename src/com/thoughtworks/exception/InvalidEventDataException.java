package com.thoughtworks.exception;

public class InvalidEventDataException extends RuntimeException {

	private static final long serialVersionUID = 2351718372759287207L;

	public InvalidEventDataException(String invalidData) {
		super(invalidData);
	}

}