package com.thoughtworks;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import com.thoughtworks.factory.DescriptionFactory;
import com.thoughtworks.factory.EventFactory;
import com.thoughtworks.filler.TrackFiller;
import com.thoughtworks.model.Event;
import com.thoughtworks.model.Track;

public class Main {

	public static void main(String[] args) throws IOException {
		Scanner in = new Scanner(System.in);

		System.out.println("Please insert path to data file:");
		String path = in.nextLine();
		List<String> lines = Files.readAllLines(
				Paths.get(path),
				Charset.defaultCharset());

		EventFactory eventFactory = new EventFactory();
		DescriptionFactory detailFactory = new DescriptionFactory();
		TrackFiller trackFiller = new TrackFiller();
		List<Event> events = new ArrayList<Event>();
		for (String line : lines) {
			events.add(eventFactory.create(line));
		}
		Collections.sort(events);

		List<Track> tracks = Arrays.asList(new Track("1"), new Track("2"));
		trackFiller.fill(tracks, events);

		System.out.println(detailFactory.getTrackDescription(tracks.get(0)));
		System.out.println("");
		System.out.println(detailFactory.getTrackDescription(tracks.get(1)));
	}

}