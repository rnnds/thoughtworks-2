package com.thoughtworks.factory;

import com.thoughtworks.exception.InvalidEventDataException;
import com.thoughtworks.model.Event;

public class EventFactory {

	private static final String MINUTES = "min";
	private static final String SEPARATOR = " ";
	private static final String LIGHTNING = "lightning";

	public Event create(String data) {
		if(data == null || data.isEmpty() || data.indexOf(SEPARATOR) == -1){
			throw new InvalidEventDataException(data);
		}
		
		return new Event(extractName(data), extractMinutes(data));
	}

	private int extractMinutes(String data) {
		String time = data.substring(data.lastIndexOf(SEPARATOR), data.length()).trim();
		if(time.equals(LIGHTNING))
			return Event.LIGHTNING_DURATION;
		
		return Integer.valueOf( time.replace(MINUTES, "") );
	}

	private String extractName(String data) {
		return data.substring(0, data.lastIndexOf(SEPARATOR));
	}

}