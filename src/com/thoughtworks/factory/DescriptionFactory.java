package com.thoughtworks.factory;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.thoughtworks.model.Event;
import com.thoughtworks.model.LunchSession;
import com.thoughtworks.model.NetworkingSession;
import com.thoughtworks.model.Session;
import com.thoughtworks.model.Track;


public class DescriptionFactory {

	public String getSessionDescription(Session session) {
		Calendar start = getStartTime(session);
		StringBuffer buffer = new StringBuffer();
		for (Event event : session.getEvents()) {
			buffer.append( getFormattedTime(start) );
			buffer.append(" ");
			buffer.append(event.getName());
			if(!session.isFixed()){
				String time = event.getMinutes() == event.LIGHTNING_DURATION ? "lightning" : event.getMinutes() + "min";
				buffer.append(" ");
				buffer.append(time);
			}
			buffer.append("\n");
			
			start.add(Calendar.MINUTE, event.getMinutes());
		}
		if(buffer.lastIndexOf("\n") != -1){
			buffer.setLength(buffer.lastIndexOf("\n"));
		}
		return buffer.toString();
	}

	private String getFormattedTime(Calendar start) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mma");
		return dateFormat.format(start.getTime());
	}

	private Calendar getStartTime(Session session) {
		Calendar startingTime = java.util.Calendar.getInstance();
		startingTime.set(Calendar.HOUR_OF_DAY, session.getStartingHour());
		startingTime.set(Calendar.MINUTE, 0);
		if(session instanceof NetworkingSession){
			Session afternoonSession = ((NetworkingSession) session).getTrack().getAfternoon(); 
			startingTime.set(Calendar.HOUR_OF_DAY, afternoonSession.getStartingHour());
			startingTime.add(Calendar.MINUTE, afternoonSession.getEventsTotalDuration());
			if(startingTime.get(Calendar.HOUR_OF_DAY) < NetworkingSession.MIN_STARTING_TIME){
				startingTime.set(Calendar.HOUR_OF_DAY, NetworkingSession.MIN_STARTING_TIME);
				startingTime.add(Calendar.MINUTE, 0);	
			}
		}
		return startingTime;
	}

	public String getTrackDescription(Track track) {
		StringBuffer buffer = new StringBuffer("Track " + track.getName() + ":\n");
		for (Session session : track.getSessions()) {
			String sessionDetail = getSessionDescription(session);
			if(sessionDetail.trim().length() > 0){
				buffer.append(sessionDetail);
				buffer.append("\n");
			}
		}
		buffer.setLength(buffer.lastIndexOf("\n"));
		return buffer.toString();
	}

}