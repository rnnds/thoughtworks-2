package com.thoughtworks;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.thoughtworks.factory.DescriptionFactoryTest;
import com.thoughtworks.factory.EventFactoryTest;
import com.thoughtworks.filler.TrackFillerTest;
import com.thoughtworks.model.SessionTest;
import com.thoughtworks.model.TrackTest;

@RunWith(Suite.class)
@SuiteClasses({
	DescriptionFactoryTest.class,
	EventFactoryTest.class,
	SessionTest.class,
	TrackTest.class,
	TrackFillerTest.class
})
public class AllTests {

}
