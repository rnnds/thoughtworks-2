package com.thoughtworks.model;

import junit.framework.Assert;

import org.junit.Test;

import com.thoughtworks.model.Event;
import com.thoughtworks.model.Session;

public class SessionTest {
	
	@Test
	public void shouldReturnSessionWith120Minutes(){
		Session session = new MorningSession();
		session.add(new Event("Name", 60));
		Assert.assertEquals(120, session.getRemainingTime());
	}
	
	@Test
	public void shouldReturnSessionWith10Minutes(){
		Session session = new MorningSession();
		session.add(new Event("Name", 60));
		session.add(new Event("Name", 60));
		session.add(new Event("Name", 50));
		Assert.assertEquals(10, session.getRemainingTime());
	}

	@Test
	public void shouldReturnSessionFull(){
		Session session = new MorningSession();
		session.add(new Event("Name", 60));
		session.add(new Event("Name", 60));
		session.add(new Event("Name", 60));
		session.add(new Event("Name", 60));
		session.add(new Event("Name", 60));
		Assert.assertTrue(session.isFull());
	}

	@Test
	public void shouldNotExceedSessionTime(){
		Session session = new MorningSession();
		session.add(new Event("Name", 60));
		session.add(new Event("Name", 60));
		session.add(new Event("Name", 60));
		session.add(new Event("Name", 60));
		session.add(new Event("Name", 60));
		Assert.assertFalse(session.add(new Event("Name", 60)));
	}
	
}