package com.thoughtworks.model;

import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

public class TrackTest {

	private Track track;
	
	@Before
	public void setUp(){
		track = new Track();
	}
	
	@Test
	public void shouldReturn4Sessions() {
		List<Session> sessions = track.getSessions();
		Assert.assertEquals(4, sessions.size());
	}
	
	@Test
	public void shouldReturnMorningSessionWith1Event() {
		track.addEvent(new Event("ABC", 10));
		Assert.assertEquals(1, track.getMorning().getEvents().size());
	}
	
	@Test
	public void shouldReturnAfternoonSessionWith2Events() {
		//MORNING
		track.addEvent(new Event("ABC", 60));
		track.addEvent(new Event("BCD", 60));
		track.addEvent(new Event("DFG", 60));
		
		//AFTERNOON
		track.addEvent(new Event("XYZ", 10));
		track.addEvent(new Event("ZZZ", 20));
		Assert.assertEquals(2, track.getAfternoon().getEvents().size());
	}
	
	@Test
	public void shouldReturnFullTrack() {
		fillTrack();
		Assert.assertTrue(track.isFull());
	}
	
	@Test
	public void shouldAddExtraEvents() {
		fillTrack();
		track.addExtraEvent(new Event("Extra", 10));
		Assert.assertEquals(1, track.getExtra().getEvents().size());
	}
	
	private void fillTrack() {
		track.addEvent(new Event("A", 60));
		track.addEvent(new Event("B", 60));
		track.addEvent(new Event("C", 60));
		track.addEvent(new Event("D", 60));
		track.addEvent(new Event("E", 60));
		track.addEvent(new Event("F", 60));
		track.addEvent(new Event("G", 60));
	}
	
}