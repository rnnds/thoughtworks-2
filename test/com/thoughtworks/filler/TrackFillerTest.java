package com.thoughtworks.filler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.thoughtworks.exception.NotEnoughTracksException;
import com.thoughtworks.exception.TrackFillerException;
import com.thoughtworks.model.Event;
import com.thoughtworks.model.Session;
import com.thoughtworks.model.Track;

public class TrackFillerTest {

	private TrackFiller trackFiller;
	
	@Before
	public void setUp(){
		trackFiller = new TrackFiller();
	}
	
	@Test(expected = TrackFillerException.class)
	public void shouldThrowExceptionForNullTracks() {
		trackFiller.fill(null, new ArrayList<Event>());
	}
	
	@Test(expected = TrackFillerException.class)
	public void shouldThrowExceptionForEmptyTracks() {
		trackFiller.fill(new ArrayList<Track>(), null);
	}
	
	@Test
	public void shouldNotThrowExceptionForEmptyEvents() {
		try {
			trackFiller.fill(Arrays.asList(new Track()), new ArrayList<Event>());
		} catch (Exception e) {
			Assert.fail();
		}
	}
	
	@Test
	public void shouldReturnFullTrack() {
		Track track = new Track();
		trackFiller.fill(Arrays.asList(track), getEvents());
		Assert.assertTrue(track.isFull());
	}
	
	@Test
	public void shouldReturnFullTracks() {
		Track track1 = new Track();
		Track track2 = new Track();
		List<Event> events = getEvents();
		events.addAll(getEvents());
		trackFiller.fill(Arrays.asList(track1, track2), events);
		Assert.assertTrue(track1.isFull() && track2.isFull());
	}
	
	@Test(expected = NotEnoughTracksException.class)
	public void shouldThrowException() {
		Track track1 = new Track();
		Track track2 = new Track();
		List<Event> events = getEvents();
		events.addAll(getEvents());
		events.addAll(getEvents());
		events.addAll(getEvents());
		trackFiller.fill(Arrays.asList(track1, track2), events);			
		Assert.fail();
	}
	
	@Test
	public void shouldNotExceedSessionDuration() {
		Track track = new Track();
		List<Event> events = Arrays.asList(
				new Event("A", 60),
				new Event("B", 60),
				new Event("C", 45),
				new Event("D", 45)
				);
		trackFiller.fill(Arrays.asList(track), events);
		Session morning = track.getMorning();
		Assert.assertTrue(morning.getRemainingTime() >= 0);
	}

	private List<Event> getEvents() {
		return new ArrayList<Event>(Arrays.asList(
				new Event("A", 60),
				new Event("B", 60),
				new Event("C", 60),
				new Event("D", 60),
				new Event("E", 60),
				new Event("F", 60),
				new Event("G", 60)
			));
	}
	
}