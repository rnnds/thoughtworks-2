package com.thoughtworks.factory;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.thoughtworks.exception.InvalidEventDataException;
import com.thoughtworks.factory.EventFactory;
import com.thoughtworks.model.Event;

public class EventFactoryTest {

	private EventFactory eventFactory;
	
	@Before
	public void setUp() throws Exception {
		eventFactory = new EventFactory();
	}

	@Test(expected = InvalidEventDataException.class)
	public void shouldReturnExceptionForNullData() {
		eventFactory.create(null);
	}
	
	@Test(expected = InvalidEventDataException.class)
	public void shouldReturnExceptionForEmptyData() {
		eventFactory.create("");
	}
	
	@Test(expected = InvalidEventDataException.class)
	public void shouldReturnExceptionForDatatWithoutWhiteSpace() {
		eventFactory.create("ABC");
	}
	
	@Test
	public void shouldReturnEventNamedABC() {
		Event event = eventFactory.create("ABC 10min");
		Assert.assertEquals("ABC", event.getName());
	}
	
	@Test
	public void shouldReturn10MinutesEvent() {
		Event event = eventFactory.create("ABC 10min");
		Assert.assertEquals(10, event.getMinutes());
	}
	
	@Test
	public void shouldReturnLightningEvent() {
		Event event = eventFactory.create("ABC lightning");
		Assert.assertEquals(Event.LIGHTNING_DURATION, event.getMinutes());
	}
	
}