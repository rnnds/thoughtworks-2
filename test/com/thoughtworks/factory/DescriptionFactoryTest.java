package com.thoughtworks.factory;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.thoughtworks.model.Event;
import com.thoughtworks.model.MorningSession;
import com.thoughtworks.model.Session;
import com.thoughtworks.model.Track;

public class DescriptionFactoryTest {
	
	private DescriptionFactory descriptionFactory;

	@Before
	public void setUp() throws Exception {
		descriptionFactory = new DescriptionFactory();
	}

	@Test
	public void shouldReturnSessionDetailWith1Event() {
		Session session = new MorningSession();
		session.add(new Event("ABC", 60));
		String detail = descriptionFactory.getSessionDescription(session);
		Assert.assertEquals("09:00AM ABC 60min", detail);
	}
	
	@Test
	public void shouldReturnSessionDetailWith2Events() {
		Session session = new MorningSession();
		session.add(new Event("ABC", 30));
		session.add(new Event("DFE", 60));
		
		String detail = descriptionFactory.getSessionDescription(session);
		Assert.assertEquals("09:00AM ABC 30min\n09:30AM DFE 60min", detail);
	}
	
	@Test
	public void shouldReturnTrackDetail() {
		Track track = new Track("1");
		track.addEvent(new Event("A", 60));
		track.addEvent(new Event("B", Event.LIGHTNING_DURATION));
		
		String detail = descriptionFactory.getTrackDescription(track);
		Assert.assertEquals(
				"Track 1:\n" +
				"09:00AM A 60min\n" +
				"10:00AM B lightning\n" +
				"12:00PM Lunch\n" +
				"04:00PM Networking Event", 
				detail);
	}
	
	@Test
	public void shouldAdaptStartingTimeToNetworking() {
		Track track = new Track("1");
		track.addEvent(new Event("A", 60));
		track.addEvent(new Event("B", 60));
		track.addEvent(new Event("C", 60));
		track.addEvent(new Event("D", 60));
		track.addEvent(new Event("E", 60));
		track.addEvent(new Event("F", 60));
		track.addEvent(new Event("G", Event.LIGHTNING_DURATION));
		
		String description = descriptionFactory.getTrackDescription(track);
		Assert.assertTrue(description.contains("04:05PM Networking Event"));
	}
	
}